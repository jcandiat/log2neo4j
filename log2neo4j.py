#!/usr/bin/env python3
#
#
# CREATE: JAVIER CANDIA TAPIA
# DATE: JULY/2020
# VERSION: 2.0
#
#
import argparse
import csv
import datetime
import uuid
import getpass
from py2neo import Graph, Node, Relationship
import sys

def initConnect(uri, user):
	try:
		password = getpass.getpass(">Enter the password to connect to the Database: ")
		driver = Graph(uri, user=user, password=password)
	except:
		print("Database connection error. Verify the information entered")
	return driver

def closeConnect(driver):
	Graph.close(driver)

def replaceCaracter(data):
	data = data.rstrip("\n")
	return data.replace("\"","")

def createNode(connect,key,label):
	node = Node(label, ip=key)
	connect.create(node)
	return node

def mergeNode(connect,key,srcuser,label):
	node = Node(label, ip=key, SourceUser=srcuser)
	merge = connect.merge(node,label,"ip")
	return node

def createRelations(keysrc,label,keydst,srcPort,Prot,dstPort,eventSubtype,connect):
	relation = Relationship(keysrc, label, keydst, SourcePort=srcPort, Protocol=Prot,DestinationPort=dstPort, EventSubtype=eventSubtype)
	connect.create(relation)

def searchValue(dictionary,value):
	if dictionary:
		valor = dictionary.values()
		if str(value) in valor:
			return False
		else:
			return True
	else:
		return True

def getIndex(dictionary,value):
	for clv,vls in dictionary.items():
		if str(value) == vls:
			return clv

def getLabel(port,evtSbt):
	try:
		protocolPort = {80 : "HTTP", 443: "HTTPS", 20: "FTP/DATA", 21: "FTP/CONTROL", 
			23: "TELNET", 22: "SSH/SCP/SFTP", 25: "SMTP", 8: "ICMP", 110: "POP3", 
			53: "DNS", 143: "IMAP", 995: "POP3/SSL", 587: "SMTP/SSL", 3306: "MySQL", 
			3389: "RDP", 1433: "SQL/Server", 7680:"DeliveryOpt/Microsoft", 3768: "VRRP",
			514:"Syslog",1701:"VPN/L2TP",67:"DHCP/Server",68:"DHCP/Client",
			88:"Kerberos",137:"Netbios/NameServices",138:"Netbios/DatagramSend",
			139:"Netbios/Sessions",161:"SNMP",162:"SNMP/TRAP",389:"LDAP",
			445:"Microsoft/DS",1080:"SOCKS/PROXY",1434:"SQL/Monitor",1512:"WINS",
			5400:"VNC",5500:"VNC",5600:"VNC",5700:"VNC",5800:"VNC",5900:"VNC",
			6667:"IRC",9050:"TOR"}
		label = protocolPort[int(port)]
	except:
		label = "TRAFFIC"
	return str(label+"/"+evtSbt.upper())

def initWorks(connect):
	connect.run("MATCH (m) DETACH DELETE m")
	for constraint in connect.run("CALL db.constraints()"):
		connect.run("DROP CONSTRAINT " + constraint[0])
	connect.run("CREATE CONSTRAINT SourceIP ON (s:SOURCE) ASSERT s.ip IS UNIQUE")
	connect.run("CREATE CONSTRAINT DestnIP ON (d:DESTINATION) ASSERT d.ip IS UNIQUE")	

def neo4jGraph(file, server, user):
	nodes = {}
	nd = 0
	graphic = initConnect(server,user)
	initWorks(graphic)
	with open(file, mode='r') as infile:
		next(infile)
		for rows in infile:
			srcIP,srcPort,srcUser,dstIP,dstPort,protocol,eventSubt = rows.split(",")
			srcIP = replaceCaracter(srcIP)
			srcPort = replaceCaracter(srcPort)
			dstIP = replaceCaracter(dstIP)
			dstPort = replaceCaracter(dstPort)
			protocol = replaceCaracter(protocol)
			eventSubt = replaceCaracter(eventSubt)
			srcUser = replaceCaracter(srcUser)
			# NODEA
			searchingA = searchValue(nodes,srcIP)
			if searchingA:
				nodeA = mergeNode(graphic,srcIP,srcUser,"SOURCE")
				nodes[nd] = str(srcIP)
				nd += 1
			else:
				nodeA = mergeNode(graphic,srcIP,srcUser,"SOURCE")
			# NODEB
			searchingB = searchValue(nodes,dstIP)
			if searchingB:
				nodeB = mergeNode(graphic,dstIP,"","DESTINATION")
				nodes[nd] = str(dstIP)
				nd += 1
			else:
				nodeB = mergeNode(graphic,dstIP,"","DESTINATION")
			# RELATION
			if str(srcIP) == "::" or str(srcIP) == "127.0.0.1":
				nodeA = nodeB
			if str(dstIP) == "::" or str(dstIP) == "127.0.0.1":
				nodeB = nodeA
			createRelations(nodeA,getLabel(dstPort,eventSubt),nodeB,srcPort,protocol,dstPort,eventSubt,graphic)

def main():
	parser = parser = argparse.ArgumentParser(description='SCRIPT that generates the nodes and relationships for import on the neo4j platform.')
	parser.add_argument('-f', '--file', help='Name of the LOG file, with the content to analyze. Format: srcIP,srcPort,dstIP,dstPort,Protocol', required=True)
	parser.add_argument('-s', '--server', help='Server. Format: bolt://localhost:7687 ', required=True)
	parser.add_argument('-u', '--user', help='Username used to connect to the database.', required=True)
	args = parser.parse_args()
	if args.file:
		neo4jGraph(args.file, args.server, args.user)

if __name__ == '__main__':
	sys.tracebacklimit=0
	main()