# Log2Neo4j

Python Script that allow migrate a file Log to Noe4j Graph Sandbox Or Desktop.

Script Details:
The following arguments are required:
-f : File
-s : Server (Connection Neo4j: JDBC)
-u : User (Default: neo4j)

The execution of the script will request the password to connect to the database.

**LIBRARY**

- argparse
- csv
- datetime
- getpass
- py2neo
- sys
